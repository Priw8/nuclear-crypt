local event = require "necro.event.Event"

event.tileSchemaBuild.add("addPalaceTiles", "modify", function(tiles)
    tiles.Floor.tilesets.Palace = {
        texture = "mods/NuclearCrypt/gfx/level/palace_floor.png"
    }

    tiles.DirtWall.tilesets.Palace = {
        texture = "mods/NuclearCrypt/gfx/level/palace_dirt.png"
    }
    tiles.DirtWallCracked.tilesets.Palace = {
        texture = "mods/NuclearCrypt/gfx/level/palace_dirt_cracked.png"
    }

    tiles.StoneWall.tilesets.Palace = {
        texture = "mods/NuclearCrypt/gfx/level/palace_stone.png"
    }
    tiles.StoneWallCracked.tilesets.Palace = {
        texture = "mods/NuclearCrypt/gfx/level/palace_stone_cracked.png"
    }

    tiles.CatacombWall.tilesets.Palace = {
        texture = "mods/NuclearCrypt/gfx/level/palace_catacomb.png"
    }
    tiles.CatacombWallCracked.tilesets.Palace = {
        texture = "mods/NuclearCrypt/gfx/level/palace_catacomb_cracked.png"
    }
end)

event.tilesetSchemaBuild.add("addPalaceTileset", "modify", function(tilesets)
    tilesets[#tilesets+1] = "Palace"
end)
