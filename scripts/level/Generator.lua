local libLevelGen = require "LibLevelGen.LibLevelGen"
local segment = require "LibLevelGen.Segment"
local room = require "LibLevelGen.Room"

local utils = require "system.utils.Utilities"
local ecs = require "system.game.Entities"

local z5bossLevel = 4 * 5
local z6bossLevel = 4 * 6

local smallRoomGenCombinations = segment.createRandLinkedRoomParameterCombinations {
    direction = {room.Direction.UP, room.Direction.DOWN, room.Direction.LEFT, room.Direction.RIGHT},
    corridorEntrance = {0.1, 0.2, 0.8, 0.9},
    corridorExit = {0.25, 0.5, 0.25},
    corridorThickness = {3},
    corridorLength = {0},
    roomWidth = {4, 5, 5, 6, 6},
    roomHeight = {4, 5, 5, 6, 6},
}

local bigRoomGenCombinations = segment.createRandLinkedRoomParameterCombinations {
    direction = {room.Direction.UP, room.Direction.DOWN, room.Direction.LEFT, room.Direction.RIGHT},
    corridorEntrance = {0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8},
    corridorExit = {0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8},
    corridorThickness = {3},
    corridorLength = {0},
    roomWidth = {7, 8, 8, 9},
    roomHeight = {7, 8, 8, 9},
}

local shopGenCombinations = segment.createRandLinkedRoomParameterCombinations {
    direction = {room.Direction.UP, room.Direction.DOWN, room.Direction.LEFT, room.Direction.RIGHT},
    corridorEntrance = {0.25, 0.5, 0.75},
    corridorExit = {0.25, 0.5, 0.75},
    corridorThickness = {1},
    corridorLength = {0},
    roomWidth = {7},
    roomHeight = {9},
}

local function initRoom(room, wallTorchCount)
    room:fill {
        tileset = "Palace",
        tileType = "Floor"
    }
    for i = 1, wallTorchCount or 1 do
        local wallTorchTile = room:chooseRandomTile {
            nearBorder = true,
            isFloor = true
        }
        wallTorchTile:convert("DirtWall")
        room:placeWallTorch(wallTorchTile)
    end
end

local function enemyLevel(enemyType, level)
    return enemyType .. (level == 1 and "" or tostring(level))
end

local function placeEnemies(room)
    local instance = room.instance
    local floor = instance:getFloor()

    local elligibleEnemyTiles = {
        isFloor = true,
        nearWall = false,
        hasEntity = false
    }
    if instance:randChance(0.3) then
        room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), "NuclearCrypt_OozeElemental")
    else
        room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), "NuclearCrypt_HellhoundOoze")
    end
    if instance:randChance(0.5) then
        room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), enemyLevel("Warlock", floor == 3 and 2 or 1))
    else
        room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), enemyLevel("Lich", instance:randIntRange(1, floor + 1)))
    end

    if instance:randChance(0.2) then
        room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), "WaterBall2")
    end

    if room.isBig then
        room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), instance:randChoice{"Slime7", "Slime8"})
        if instance:randChance(0.5) then
            room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), enemyLevel("Warlock", floor == 3 and 2 or 1))
        else
            room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), enemyLevel("Lich", instance:randIntRange(1, floor + 1)))
        end
        if instance:randChance(0.3) then
            room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), "NuclearCrypt_OozeElemental")
        else
            room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), "NuclearCrypt_HellhoundOoze")
        end
        if instance:randChance(0.3) then
            room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), "NuclearCrypt_OozeElemental")
        else
            room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), "NuclearCrypt_HellhoundOoze")
        end


        room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), enemyLevel("Skull", instance:randIntRange(1, floor + 1)))
        room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), enemyLevel("Skull", instance:randIntRange(1, floor + 1)))
        room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), enemyLevel("Monkey", instance:randIntRange(3, 5)))
    end

    if room.exit then
        room:placeEntity(room:getTile(room.exit.x, room.exit.y), instance:randChoice{"Dragon2", "Ogre", "Banshee2", "Nightmare2"})
        room:placeEntity(room:chooseRandomTile(elligibleEnemyTiles), enemyLevel("Skull", instance:randIntRange(1, floor + 1)))
    end
end

local function placeTraps(room)
    local elligibleTrapTiles = {
        isFloor = true,
        hasEntity = false
    }
    local trapTypes = {"BounceTrapOmni", "TeleportTrap", "ScatterTrap", "BombTrap", "SpikeTrap"}
    
    for i = 1, room.instance:randChance(0.2) and 2 or 1 do
        room:placeEntity(room:chooseRandomTile(elligibleTrapTiles), room.instance:randChoice(trapTypes))
    end
end

local function palaceLevelGenerator(ev)
    local instance = libLevelGen.new(ev)

    instance:setMusic {
        id = "OST",
        level = instance:getFloor(),
        type = "zone",
        zone = 4
    }

    local mainSegment = instance:createSegment()
    mainSegment:setPadding {
        mainTile = {
            tileset = "Palace",
            type = "DirtWall"
        },
        extraTiles = {
            {
                tileset = "Palace",
                type = "StoneWall",
                ratio = 0.2
            }
        },
        count = 2
    }

    local startingRoom = mainSegment:createRoom(-3, -3, 6, 6, room.RoomType.STARTING)
    initRoom(startingRoom)

    local fromStartingRoomToMainRoom = instance:randIntRange(1, 3)
    local fromMainRoomToExitRoom = 3 - fromStartingRoomToMainRoom

    local lastCorridor
    local lastRoom = startingRoom
    for i = 1, fromStartingRoomToMainRoom do
        lastCorridor, lastRoom = mainSegment:createRandLinkedRoom(lastRoom, false, smallRoomGenCombinations)
        if not lastRoom then
            error("Error creating corridor out of starting room")
        end
        initRoom(lastRoom)
    end

    local _, bigRoom = mainSegment:createRandLinkedRoom(lastRoom, false, bigRoomGenCombinations)
    bigRoom.isBig = true
    initRoom(bigRoom)

    local _, shopRoom = mainSegment:createRandLinkedRoom(bigRoom, false, shopGenCombinations)
    shopRoom:makeRoomShop("Palace")

    -- Fake path
    lastRoom = bigRoom
    for i = 1, fromStartingRoomToMainRoom do
        lastCorridor, lastRoom = mainSegment:createRandLinkedRoom(lastRoom, false, smallRoomGenCombinations)
        lastRoom:placeWallTorches(4)
        if not lastRoom then
            error("Error creating fake path")
        end
        initRoom(lastRoom)
    end

    lastRoom = bigRoom
    for i = 1, fromMainRoomToExitRoom do
        lastCorridor, lastRoom = mainSegment:createRandLinkedRoom(lastRoom, false, smallRoomGenCombinations)
        lastRoom:placeWallTorches(4)
        if not lastRoom then
            error("Error creating path to exit")
        end
        initRoom(lastRoom)
    end

    local _, exitRoom = mainSegment:createRandLinkedRoom(lastRoom, false, bigRoomGenCombinations)
    exitRoom.isBig = true
    initRoom(exitRoom)
    exitRoom:placeExit()

    local enemyRooms = mainSegment:getRoomsOfTypes {room.RoomType.REGULAR, room.RoomType.EXIT}
    for _, enemyRoom in ipairs(enemyRooms) do
        placeEnemies(enemyRoom)
        placeTraps(enemyRoom)
    end

    mainSegment:placeChests(1)

    instance:finalize()
end

libLevelGen.registerGenerator("NuclearCrypt", function (ev, orgGenerator)
    if ev.level >= z6bossLevel then
        ev.level = ev.level - 4
        orgGenerator(ev)
    elseif ev.level == z5bossLevel then
        ev.data = utils.deepCopy(ev.data)
        local bardPrototype = ecs.getEntityPrototype("Bard")
        ev.data.activeCharacters = bit.lshift(1, bardPrototype.playerXMLMapping.id)
        orgGenerator(ev)
    elseif ev.level > z5bossLevel then
        palaceLevelGenerator(ev)
    else
        orgGenerator(ev)
    end
end)

libLevelGen.registerPostProcessor(function (ev)
    if ev.num < z5bossLevel and (ev.num-1) % 4 + 1 ~= 4 then
        local tileMapping = ev.tileMapping
        tileMapping.tilesetNames[#tileMapping.tilesetNames+1] = "Palace"
        tileMapping.tilesets[#tileMapping.tilesets+1] = #tileMapping.tilesetNames
        tileMapping.tilesets[#tileMapping.tilesets+1] = #tileMapping.tilesetNames
        tileMapping.tileNames[#tileMapping.tileNames+1] = "Floor"
        tileMapping.tileNames[#tileMapping.tileNames+1] = "DirtWall"

        local palaceFloor = #tileMapping.tileNames - 1
        local palaceWall = #tileMapping.tileNames

        local instance = libLevelGen.new({
            data = {
                seed = ev.seed,
            },
            failMaps = 0
        })
        local mainSegmentData = ev.segments[1]

        local w = mainSegmentData.bounds[3]
        local x
        local y

        -- This is bad but I don't have time
        local tileType, i
        while true do
            i = instance:randIntRange(1, #mainSegmentData.tiles+1)
            tileType = mainSegmentData.tiles[i]
            if tileMapping.tileNames[tileType] == "Floor" then
                x = i % w
                y = (i - x) / w
                if math.abs(x + mainSegmentData.bounds[1]) > 8 and math.abs(y + mainSegmentData.bounds[2]) > 8 then
                    break
                end
            end
        end
    
        -- i = (w * y) + x + 1
        i = i - 1
        local centerX = x
        local centerY = y

        local minX = x - 4
        local maxX = x + 4
        local minY = y - 4
        local maxY = y + 4
        x = minX
        while x <= maxX do
            y = minY
            while y <= maxY do
                local currentTileType = mainSegmentData.tiles[(w * y) + x + 1]
                local currentTileName = tileMapping.tileNames[currentTileType]
                if currentTileName == "Floor" then
                    mainSegmentData.tiles[(w * y) + x + 1] = palaceFloor
                elseif currentTileName == "DirtWall" or currentTileName == "StoneWall" or currentTileName == "CatacombWall" then
                    mainSegmentData.tiles[(w * y) + x + 1] = palaceWall
                end
                y = y + 1
            end
            x = x + 1
        end
        ev.entities[#ev.entities+1] = {
            type = instance:randChoice{"NuclearCrypt_OozeElemental", "NuclearCrypt_HellhoundOoze"},
            x = centerX + mainSegmentData.bounds[1],
            y = centerY + mainSegmentData.bounds[2]
        }
    end
end)

libLevelGen.setDefaultGenerator("NuclearCrypt")
