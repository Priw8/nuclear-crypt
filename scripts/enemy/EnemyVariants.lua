local action = require "necro.game.system.Action"
local customEntities = require "necro.game.data.CustomEntities"
local enemy = require "necro.game.data.enemy.CommonEnemy"
local event = require "necro.event.Event"

-- OozeDog
customEntities.extend {
    name = "HellhoundOoze",
    template = customEntities.template.enemy("hellhound"),
    components = {
        sprite = {texture = "mods/NuclearCrypt/gfx/enemy/hellhound-ooze.png"},
        health = {
            health = 2,
            maxHealth = 2
        },
        castOnDeath = {
            spell = "SpellcastOoze"
        },
    }
}

-- Ok this is pretty awful
customEntities.extend {
    name = "OozeElemental",
    template = customEntities.template.enemy("iceelemental"),
    components = {
        sprite = {texture = "mods/NuclearCrypt/gfx/enemy/ooze-elemental.png"},
        innateSpellcasts = {
            mapping = {
                [action.Special.SPELL_1] = "SpellcastOoze",
            },
        }
    }
}
