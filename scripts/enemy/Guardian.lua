local components = require "necro.game.data.Components"
local collision = require "necro.game.tile.Collision"
local customEntities = require "necro.game.data.CustomEntities"
local event = require "necro.event.Event"
local targeting = require "necro.game.enemy.Targeting"
local object = require "necro.game.object.Object"
local move = require "necro.game.system.Move"

local field = components.field
local constant = components.constant

-- Unfinished because I didn't have time to make an actual new enemy type

local phase = {
    teleport = 1,
    shoot = 2,
}

components.register {
    Guardian = {
        field.int8("teleportTimer", 5),
        field.int8("shootTimer", 2),
        field.int8("phase", phase.teleport)
    }
}

local guardianAI = 13129

customEntities.extend {
    name = "Guardian",
    template = customEntities.template.enemy(),
    components = {
        sprite = {
            texture = "mods/NuclearCrypt/gfx/enemy/guardian.png",
            width = 60,
            height = 60
        },
        positionalSprite = {
            offsetX = -20,
            offsetY = -30,
        },
        normalAnimation = { frames = {1, 2, 3, 4} },
        health = {
            maxHealth = 5,
            health = 5
        },
        healthBar = {
            offsetY = -20
        },
        beatDelay = {
            interval = 1,
        },
        ai = {
            id = guardianAI,
        },
        NuclearCrypt_Guardian = {}
    }
}

event.aiAct.add("GuardianAI", guardianAI, function (ev)
    local guardian = ev.entity.NuclearCrypt_Guardian
    if guardian.phase == phase.shoot then

    else
        guardian.teleportTimer = guardian.teleportTimer - 1
        if guardian.teleportTimer == 0 then
            local newTarget = targeting.getNearestHostileEntity(ev.entity.position.x, ev.entity.position.y, ev.entity.team.id)
            targeting.setTarget(ev.entity, newTarget)
            local dx = newTarget.position.x - newTarget.previousPosition.x
            local dy = newTarget.position.y - newTarget.previousPosition.y
            if dx == 0 and dy == 0 then
                dx = 1
            end

            local spawnX = newTarget.position.x + dx * 4
            local spawnY = newTarget.position.y + dy * 4
            if not collision.check(spawnX, spawnY, collision.Type.SOLID) then
                move.absolute(ev.entity, spawnX, spawnY, move.Type.TELEPORT)
                guardian.phase = phase.shoot
            else
                guardian.teleportTimer = 1
            end
        end
    end
end)
